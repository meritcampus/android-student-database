package com.example.addstudentdatatodatabase;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	EditText firstName, marks, section;
	Button addButton;
	TextView resultText;
DataBaseHelper dbHelper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		firstName = (EditText) findViewById(R.id.name_edittext);
		marks = (EditText) findViewById(R.id.marks_edittext);
		section = (EditText) findViewById(R.id.section_edittext);
		addButton = (Button) findViewById(R.id.add_button);
		resultText = (TextView) findViewById(R.id.resultText);
		addButton.setOnClickListener(this);
		dbHelper=new DataBaseHelper(this);
		dbHelper.getAllDetails();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if(firstName.getText().toString().length() >0&&marks.getText().toString().length() >0&&section.getText().toString().length() >0){
				
				Student student=new Student();
				student.setName(firstName.getText().toString());
				student.setMarks(Integer.parseInt(marks.getText().toString()));
				student.setSection(section.getText().toString().charAt(0));
				
				dbHelper.addStudentDetails(student);
				Toast.makeText(getApplicationContext(), "Inserted student details successfully", Toast.LENGTH_SHORT).show();
			}else{
				
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}

			break;

		default:
			break;
		}
	}
}